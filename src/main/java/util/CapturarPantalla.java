package util;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.TakesScreenshot;

public class CapturarPantalla{
	
	File screenshot;	
		
	public void takeScreenshot(WebDriver driver, String nArchivo){
		try {			
			Thread.sleep(6000);
			screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {				
				String pathFile="./target/Screenshots";
    			FileUtils.copyFile(screenshot, new File(pathFile+"/"+nArchivo + " " + getTimeStampValue() + ".jpg"));			
    		} catch (IOException e) {    			
    			e.printStackTrace();
    		}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}		
	}
	
	public  String getTimeStampValue()throws IOException{
	    Calendar cal = Calendar.getInstance();       
	    java.util.Date time= cal.getTime();
	    String timestamp= time.toString();
	    String systime=timestamp.replace(":", "-");
	    //System.out.println(systime);
	    return systime;
	}

}

